import Proj1 from "../assets/images/calculadora-simples.png";
import Proj2 from "../assets/images/dsmeta.png";
import Proj3 from "../assets/images/email.png";
import Proj4 from "../assets/images/site-django.jpeg";

export const ProjectList = [
    {
        name: "Calculadora simples",
        image: Proj1,
        stacks: "Java, Maven, JavaFx",
        repositoryLink: "https://gitlab.com/devops1817/simple-calculator",
        linkVideo: "https://www.youtube.com/watch?v=Pm8x3tcAREA",
        linkDeploy: "",
    },
    {
        name: "Gerenciamento de vendas e envio de sms",
        image: Proj2,
        stacks: "Spring Boot, React, Java, HTML, CSS, Typescript",
        repositoryLink: "https://gitlab.com/IgorGuilhermeRocha/semana-spring-react",
        linkVideo: "",
        linkDeploy: "https://dsmeta-igor-rocha.netlify.app/",
    },
    {
        name: "Sistema de email",
        image: Proj3,
        stacks: "Java, Maven, JavaFx, Mysql",
        repositoryLink: "https://gitlab.com/IgorGuilhermeRocha/sockets",
        linkVideo: "https://www.youtube.com/watch?v=C9j30MuOXzE",
        linkDeploy: "",
    },
    {
        name: "Portfólio",
        image: Proj4,
        stacks: "HTML, CSS, Python, Django",
        repositoryLink: "https://gitlab.com/devops1817/igorguilhermerocha",
        linkVideo: "",
        linkDeploy: "https://dashboard.heroku.com/apps/igorguilhermerocha",
    },
];