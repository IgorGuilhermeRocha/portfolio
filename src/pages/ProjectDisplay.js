import React from "react";
import { useParams } from "react-router-dom";
import { ProjectList } from "../helpers/ProjectList";
import "../styles/ProjectDisplay.css";


function ProjectDisplay() {
    const { id } = useParams();
    const project = ProjectList[id];

    return (
        <div className="project">
            <h1> {project.name}</h1>
            <img src={project.image} alt="" />
            <p>
                <b>Stacks: </b> {project.stacks} <br />
                <b> Link vídeo: </b> <a href={project.linkVideo}> {project.linkVideo} </a> <br />
                <b> Link repositório: </b> <a href={project.repositoryLink}> {project.repositoryLink}</a> <br />
                <b> Link deploy:  </b> <a href={project.linkDeploy}> {project.linkDeploy} </a>  <br />
            </p>
        </div >
    );
}

export default ProjectDisplay;

