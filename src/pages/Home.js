import React from "react";
import "../styles/Home.css";

function Home() {
    return (
        <div className="home">
            <div className="about">
                <h2> Igor Guilherme Almeida Rocha</h2>
                <div className="prompt">
                    <p>Desenvolvedor júnior</p>
                </div>
            </div>

            <div className="skills">
                <h1> Stacks</h1>
                <ol className="list">
                    <li className="item">
                        <h2> Front-End</h2>
                        <span>
                            HTML, CSS, JavaScript, ReactJs, Yarn.
                        </span>
                    </li>
                    <li className="item">
                        <h2>Back-End</h2>
                        <span>
                            Java Spring, JavaFx, Maven, Django, MySql, PostgreSql.
                        </span>
                    </li>

                    <li className="item">
                        <h2>Linguagens</h2>
                        <span>JavaScript, Java, Python, TypeScript</span>
                    </li>

                    <li className="item">
                        <h2>Outras tecnologias</h2>
                        <span>Docker, Gitlab CI/CD</span>
                    </li>
                </ol>
            </div>
        </div>
    )
}

export default Home;