import React from 'react';
import InstagramIcon from '@material-ui/icons/Instagram';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

import "../styles/Footer.css"

function Footer() {
    const linkedInUrl = "https://www.linkedin.com/in/igor-guilherme-945525214/";
    const instagramUrl = "https://www.instagram.com/igor_guilherme_rocha/";

    return (
        <div className='footer'>
            <div className='footer-icons'>
                <a href={instagramUrl}><InstagramIcon /></a>
                <a href={linkedInUrl}><LinkedInIcon /></a>
            </div>
            <p> &copy; 2022 personal-website-igor-rocha</p>
        </div>

    )
}

export default Footer;