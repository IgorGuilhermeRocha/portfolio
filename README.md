# Portfólio

Este é um projeto feito com objetivo de ser um portfólio, onde eu poderei mostrar os meus projetos.

## Tecnologias 
React, HTML, CSS, JavaScript

## Deploy 
https://personal-website-igor-rocha.netlify.app/

## Referências

Para a construção deste site utilizei como base este vídeo: https://www.youtube.com/watch?v=x7mwVn2z3Sk